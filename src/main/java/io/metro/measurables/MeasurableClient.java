/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.metro.measurables.models.*;
import io.metro.specification.Filter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.geo.Metrics;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

import static org.hibernate.validator.internal.util.Contracts.assertValueNotNull;

@Service
public class MeasurableClient {
    private static final Logger logger = LoggerFactory.getLogger(MeasurableClient.class);

    @Value("${spring.api.gateway-service}")
    private String SERVICE;
    @Value("${spring.api.username}")
    private String username;
    @Value("${spring.api.password}")
    private String password;

    private final int DEFAULT_PAGINATION_PAGE = 0;
    private final int DEFAULT_PAGINATION_SIZE = 10;
    private final int DEFAULT_GEO_DISTANCE = 50;
    private final Metrics DEFAULT_GEO_METRIC = Metrics.KILOMETERS;


    private static final String BUILDINGS = "/buildings";
    private static final String MEASURABLES = "/measurables";
    private static final String CLIMATE   = "/climate";

    private RestTemplate restTemplate;
    private HttpHeaders headers;

    @Autowired
    public MeasurableClient(@LoadBalanced RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
        this.headers = new HttpHeaders();
    }

    public MeasurableClient init() {
        System.out.println("MeasurableClient PostConstruct Initialized");
        logger.debug("");
        logger.debug("====================================================================================");
        logger.debug("========================== initializing MeasurableClient ===========================");
        logger.debug("Authenticating Client with username: {} and password: {}", username, password);
        authenticate(username, password);
        logger.debug("Setting header X-Forwarded-Host: {}", getXForwardedHost(SERVICE));
        headers.set("X-Forwarded-Host", getXForwardedHost(SERVICE));
        logger.debug("===========================   initialization completed   ===========================");
        logger.debug("");

        return this;
    }

    private String getXForwardedHost(String url) {
        if(url.toLowerCase().contains("https://")) {
            return url.toLowerCase().replaceFirst("https://", "");
        }
        else if (url.toLowerCase().contains("http://")) {
            return url.toLowerCase().replaceFirst("http://", "");
        }

        return url;
    }

    public MeasurableClient authenticate(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String( encodedAuth );
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, authHeader);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        HttpEntity<Object> response = restTemplate.exchange(SERVICE, HttpMethod.GET, httpEntity, Object.class);

        String sessionToken = response.getHeaders().getFirst("x-auth-token");
        assertValueNotNull(sessionToken, "Response: x-auth-token");

        withAuthToken(sessionToken);
        logger.debug("Setting header X-Auth-Token: {}", sessionToken);

        return this;
    }

    /**
     * Override the URL of the service to use.
     * Note: URL is stripped of trailing slash.
     *
     * @param url URL of service
     * @return this client
     */
    public MeasurableClient withUrl(String url) {
        SERVICE = url.substring(0, url.length() - (url.endsWith("/") ? 1 : 0));
        return this;
    }

    public MeasurableClient withAuthToken(String auth) {
        headers.set("x-auth-token", auth);
        return this;
    }

    /**
     * Request a {@link Measurable} by id
     *
     * @param id The meter id
     * @return {@link Measurable}
     */
    public ResponseEntity<Resource<Measurable>> getMeasurable(String id) {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURABLES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<Resource<Measurable>>() {});
    }

    /**
     * Returns all of the {@link Measurable}'s associated with a building id
     *
     * @param id the building id
     * @return collection of {@link Measurable}
     */
    public ResponseEntity<MetroPagedResources<Resource<Measurable>>>
    getBuildingMeasurables(String id) {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(MEASURABLES)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<MetroPagedResources<Resource<Measurable>>>() {});
    }

    /**
     * Request a collection of {@link Measurable} for a given building
     *
     * @param id the building id
     * @param page the pagination offset
     * @param size the number records to return
     * @return {@link Measurable}
     */
    public ResponseEntity<MetroPagedResources<Resource<Measurable>>>
    getBuildingMeasurables(String id, int page, int size) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(MEASURABLES)
                .queryParam("page", page)
                .queryParam("size", size)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<MetroPagedResources<Resource<Measurable>>>() { });

    }

    public ResponseEntity<MetroPagedResources<Resource<Measurable>>>
    findMeasurables(Filter filter, int page, int size) {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURABLES)
                .path("/search")
                .queryParam("page", page)
                .queryParam("size", size)
                .build();

        return restTemplate.exchange(builder.toString(), HttpMethod.POST, new HttpEntity<>(filter, headers),
                new ParameterizedTypeReference<MetroPagedResources<Resource<Measurable>>>() {});
    }

    /**
     * Persist a {@link Measurable}
     *
     * @param entity {@link Measurable}
     * @return {@link Measurable}
     */
    public ResponseEntity createMeasurable(Measurable entity) {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURABLES)
                .build(true);

        return restTemplate.exchange(builder.toString(), HttpMethod.POST, new HttpEntity<>(entity, headers),
                new ParameterizedTypeReference<Resources<Measurable>>() { });
    }

    /**
     * Request deletion of a {@link Measurable}
     *
     * @param id the meter id
     */
    public ResponseEntity deleteMeasurable(String id) {

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(MEASURABLES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.DELETE,
                new HttpEntity<>(headers), new ParameterizedTypeReference<Resources<Measurable>>() { });
    }


    /*
     * Climate Zone Station Data Endpoints
     */
    /**
     * Find nearest N {@link ClimateZone}'s
     * Defaults to 50 KILOMETERS and limits returns to the top 10
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @return Returns collection of {@link ClimateZone}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<ClimateZone>>>
    findNearestClimateZone(double latitude, double longitude) {
        return findNearestClimateZone(latitude, longitude, DEFAULT_PAGINATION_PAGE, DEFAULT_PAGINATION_SIZE,
                DEFAULT_GEO_DISTANCE, DEFAULT_GEO_METRIC);
    }

    /**
     * Find nearest N {@link ClimateZone}'s
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @param page pagination offset
     * @param size number records to return
     * @param distance the radius within which to limit results
     * @param metric the metric to measure distance in
     * @return Returns collection of {@link ClimateZone}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<ClimateZone>>>
    findNearestClimateZone(double latitude, double longitude, int page, int size, int distance, Metrics metric) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/zone")
                .queryParam("latitude", latitude)
                .queryParam("longitude", longitude)
                .queryParam("page", page)
                .queryParam("size", size)
                .queryParam("distance", distance)
                .queryParam("metric", metric)
                .build(true);

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateZone>>> () {});
    }

    /**
     *
     * @param id the identifier
     * @return {@link ClimateZone}
     */
    public ResponseEntity<Resource<ClimateZone>>
    getClimateZone(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/zone/{id}")
                .buildAndExpand(id);

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resource<ClimateZone>> () {});
    }

    /*
     * Energy Plus Station Data Endpoints
     */
    /**
     * Find nearest N {@link EpwStation}'s
     * Defaults to 50 KILOMETERS and limits returns to the top 10
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @return Returns collection of {@link EpwStation}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<EpwStation>>>
    getNearestEnergyPlusStation(double latitude, double longitude) {
        return getNearestEnergyPlusStation(latitude, longitude, DEFAULT_PAGINATION_PAGE, DEFAULT_PAGINATION_SIZE,
                DEFAULT_GEO_DISTANCE, DEFAULT_GEO_METRIC);
    }

    /**
     * Find nearest N {@link EpwStation}'s
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @param page pagination offset
     * @param size number records to return
     * @param distance the radius within which to limit results
     * @param metric the metric to measure distance in
     * @return Returns collection of {@link EpwStation}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<EpwStation>>>
    getNearestEnergyPlusStation(double latitude, double longitude, int page, int size, int distance, Metrics metric) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/energy-plus-station")
                .queryParam("latitude", latitude)
                .queryParam("longitude", longitude)
                .queryParam("page", page)
                .queryParam("limit", size)
                .queryParam("distance", distance)
                .queryParam("metric", metric)
                .build(true);

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<EpwStation>>>() {});
    }

    /**
     *
     * @param id the station identifier
     * @return {@link EpwStation}
     */
    public ResponseEntity<Resource<EpwStation>>
    getEnergyPlusWeatherStation(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/energy-plus-station/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resource<EpwStation>>() {});
    }

    /*
     * NOAA Station Data Endpoints
     */
    /**
     * Find nearest N {@link NoaaStation}'s
     * Defaults to 50 KILOMETERS and limits returns to the top 10
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @return Returns collection of {@link NoaaStation}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<NoaaStation>>>
    getNearestNoaaStation(double latitude, double longitude) {
        return getNearestNoaaStation(latitude, longitude, DEFAULT_PAGINATION_PAGE, DEFAULT_PAGINATION_SIZE,
                DEFAULT_GEO_DISTANCE, DEFAULT_GEO_METRIC);
    }

    /**
     * Find nearest N {@link NoaaStation}'s
     *
     * @param latitude the latitude
     * @param longitude the longitude
     * @param page pagination offset
     * @param size number records to return
     * @param distance the radius within which to limit results
     * @param metric the metric to measure distance in
     * @return Returns collection of {@link NoaaStation}'s ordered from nearest to farthest
     */
    public ResponseEntity<PagedResources<Resource<NoaaStation>>>
    getNearestNoaaStation(double latitude, double longitude, int page, int size, int distance, Metrics metric) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station")
                .queryParam("latitude", latitude)
                .queryParam("longitude", longitude)
                .queryParam("page", page)
                .queryParam("limit", size)
                .queryParam("distance", distance)
                .queryParam("metric", metric)
                .build(true);

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<NoaaStation>>>() {});
    }

    /**
     *
     * @param id the station identifier
     * @return {@link NoaaStation}
     */
    public ResponseEntity<Resource<NoaaStation>>
    getNoaaWeatherStation(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET,
                new HttpEntity<>(headers), new ParameterizedTypeReference<Resource<NoaaStation>>() { });
    }



    /*
     * Hourly NOAA Climate Data Endpoints
     */
    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getHourlyClimateData(String id, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/hourly-data")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getHourlyClimateDataBeforeDate(String id, LocalDate date, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/hourly-data/before/{date}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, date)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getHourlyClimateDataAfterDate(String id, Date date, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/hourly-data/after/{date}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, date)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getHourlyClimateDataForYear(String id, int year, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/hourly-data/{year}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, year)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }



    /*
     * Monthly NOAA Climate Data Endpoints
     */
    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getMonthlyClimateData(String id, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/monthly-data")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getMonthlyClimateDataBeforeDate(String id, LocalDate date, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/monthly-data/before/{date}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, date)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getMonthlyClimateDataAfterDate(String id, LocalDate date, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/monthly-data/after/{date}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, date)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    public ResponseEntity<PagedResources<Resource<ClimateData>>>
    getMonthlyClimateDataForYear(String id, int year, int page, int limit) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/noaa-station/{id}/monthly-data/{year}")
                .queryParam("page", page)
                .queryParam("limit", limit)
                .buildAndExpand(id, year)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<ClimateData>>>() {});
    }

    /**
     * Get the calculated degree day data for a year
     *
     * @param id the noaa station id
     * @param year the year of data to return
     * @param hbp the heating balance point in °C
     * @param cbp the cooling balance point in °C
     * @return DegreeDay's
     */
    public ResponseEntity<Collection<DegreeDay>> getDegreeDaysForYear(String id, int year, double hbp, double cbp) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(CLIMATE)
                .path("/{station_id}/monthly/{year}/degreeDays")
                .queryParam("hbp", hbp)
                .queryParam("cbp", cbp)
                .buildAndExpand(id, year)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Collection<DegreeDay>>() { });
    }


    public static <T> T fromJson(String jsonString, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule());
        try {
            return mapper.readValue(jsonString, clazz);
        } catch (IOException e) {}

        return null;
    }
}
