/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Resource} wrapping a domain object and adding links to it.
 *
 * @author Oliver Gierke
 */
@XmlRootElement
public class MetroResource<T> extends ResourceSupport {

    private final T content;
    private final Map<String, Object> metaProperties = new HashMap<>();

    /**
     * Creates an empty {@link Resource}.
     */
    MetroResource() {
        this.content = null;
    }

    /**
     * Creates a new {@link Resource} with the given content and {@link Link}s (optional).
     *
     * @param content must not be {@literal null}.
     * @param links the links to add to the {@link Resource}.
     */
    public MetroResource(T content, Link... links) {
        this(content, Arrays.asList(links));
    }

    /**
     * Creates a new {@link Resource} with the given content and {@link Link}s.
     *
     * @param content must not be {@literal null}.
     * @param links the links to add to the {@link Resource}.
     */
    public MetroResource(T content, Iterable<Link> links) {

        Assert.notNull(content, "Content must not be null!");
        Assert.isTrue(!(content instanceof Collection), "Content must not be a collection! Use Resources instead!");
        this.content = content;
        this.add(links);
    }

    /**
     * Returns the underlying entity.
     *
     * @return the content
     */
    @JsonUnwrapped
    @XmlAnyElement
    public T getContent() {
        return content;
    }

    @JsonProperty("metadata")
    public Map<String, Object> getMetaProperties() {
        return metaProperties;
    }

    /*
         * (non-Javadoc)
         * @see org.springframework.hateoas.ResourceSupport#toString()
         */
    @Override
    public String toString() {
        return String.format("Resource { content: %s, %s }", getContent(), super.toString());
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.hateoas.ResourceSupport#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !obj.getClass().equals(getClass())) {
            return false;
        }

        MetroResource<?> that = (MetroResource<?>) obj;

        boolean contentEqual = this.content == null ? that.content == null : this.content.equals(that.content);
        return contentEqual ? super.equals(obj) : false;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.hateoas.ResourceSupport#hashCode()
     */
    @Override
    public int hashCode() {

        int result = super.hashCode();
        result += content == null ? 0 : 17 * content.hashCode();
        return result;
    }
}
