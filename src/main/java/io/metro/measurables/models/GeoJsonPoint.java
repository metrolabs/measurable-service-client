/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import java.util.List;

public class GeoJsonPoint {
    private double x;
    private double y;
    private String type;
    private List<Double> coordinates;

    public double getX() {
        return x;
    }

    public GeoJsonPoint setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return y;
    }

    public GeoJsonPoint setY(double y) {
        this.y = y;
        return this;
    }

    public String getType() {
        return type;
    }

    public GeoJsonPoint setType(String type) {
        this.type = type;
        return this;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public GeoJsonPoint setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
        return this;
    }
}
