/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ClimateData {
    private String id;
    private String stationId;
    private Date observationDate;
    private int observationYear;
    private int observationMonth;
    private int observationDay;
    private int observationHour;
    private Double airTempCelsius;
    private Double airTempFahrenheit;
    private String dataSource;

    public String getId() {
        return id;
    }

    public ClimateData setId(String id) {
        this.id = id;
        return this;
    }

    public String getStationId() {
        return stationId;
    }

    public ClimateData setStationId(String stationId) {
        this.stationId = stationId;
        return this;
    }

    public Date getObservationDate() {
        return observationDate;
    }

    public ClimateData setObservationDate(Date observationDate) {
        this.observationDate = observationDate;
        return this;
    }

    public int getObservationYear() {
        return observationYear;
    }

    public ClimateData setObservationYear(int observationYear) {
        this.observationYear = observationYear;
        return this;
    }

    public int getObservationMonth() {
        return observationMonth;
    }

    public ClimateData setObservationMonth(int observationMonth) {
        this.observationMonth = observationMonth;
        return this;
    }

    public int getObservationDay() {
        return observationDay;
    }

    public ClimateData setObservationDay(int observationDay) {
        this.observationDay = observationDay;
        return this;
    }

    public int getObservationHour() {
        return observationHour;
    }

    public ClimateData setObservationHour(int observationHour) {
        this.observationHour = observationHour;
        return this;
    }

    public Double getAirTempCelsius() {
        return airTempCelsius;
    }

    public ClimateData setAirTempCelsius(Double airTempCelsius) {
        this.airTempCelsius = airTempCelsius;
        return this;
    }

    public Double getAirTempFahrenheit() {
        return airTempFahrenheit;
    }

    public ClimateData setAirTempFahrenheit(Double airTempFahrenheit) {
        this.airTempFahrenheit = airTempFahrenheit;
        return this;
    }

    public String getDataSource() {
        return dataSource;
    }

    public ClimateData setDataSource(String dataSource) {
        this.dataSource = dataSource;
        return this;
    }
}
