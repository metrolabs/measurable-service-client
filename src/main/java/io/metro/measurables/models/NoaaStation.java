/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.metro.measurables.CustomDateDeserializer;
import org.springframework.data.geo.Distance;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NoaaStation {
    private String id;
    private String usaf;
    private String wban;
    private String stationName;
    private String country;
    private String region;
    private GeoJsonPoint location;
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date inventoryStart;
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date inventoryEnd;
    private long inventoryYears;
    private int inventoryStartYear;
    private int inventoryEndYear;
    private Distance distance;

    public NoaaStation(){}

    public String getId() {
        return id;
    }

    public NoaaStation setId(String id) {
        this.id = id;
        return this;
    }

    public String getUsaf() {
        return usaf;
    }

    public NoaaStation setUsaf(String usaf) {
        this.usaf = usaf;
        return this;
    }

    public String getWban() {
        return wban;
    }

    public NoaaStation setWban(String wban) {
        this.wban = wban;
        return this;
    }

    public String getStationName() {
        return stationName;
    }

    public NoaaStation setStationName(String stationName) {
        this.stationName = stationName;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public NoaaStation setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public NoaaStation setRegion(String region) {
        this.region = region;
        return this;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public NoaaStation setLocation(GeoJsonPoint location) {
        this.location = location;
        return this;
    }

    public Date getInventoryStart() {
        return inventoryStart;
    }

    public NoaaStation setInventoryStart(Date inventoryStart) {
        this.inventoryStart = inventoryStart;
        return this;
    }

    public Date getInventoryEnd() {
        return inventoryEnd;
    }

    public NoaaStation setInventoryEnd(Date inventoryEnd) {
        this.inventoryEnd = inventoryEnd;
        return this;
    }

    public long getInventoryYears() {
        return inventoryYears;
    }

    public NoaaStation setInventoryYears(long inventoryYears) {
        this.inventoryYears = inventoryYears;
        return this;
    }

    public int getInventoryStartYear() {
        return inventoryStartYear;
    }

    public NoaaStation setInventoryStartYear(int inventoryStartYear) {
        this.inventoryStartYear = inventoryStartYear;
        return this;
    }

    public int getInventoryEndYear() {
        return inventoryEndYear;
    }

    public NoaaStation setInventoryEndYear(int inventoryEndYear) {
        this.inventoryEndYear = inventoryEndYear;
        return this;
    }

    public Distance getDistance() {
        return distance;
    }

    public NoaaStation setDistance(Distance distance) {
        this.distance = distance;
        return this;
    }
}
