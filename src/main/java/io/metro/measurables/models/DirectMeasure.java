/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Timestamp;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DirectMeasure {
    private String id;
    private String accountId;
    private String meterId;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    /**
     * Duration measurement period in minutes
     */
    private int duration;
    /**
     * The timestamp marking the time this system processed and persisted this event
     */
    private Timestamp timestamp;
    private double usage;
    private double cost;
    private double usageKWH;
    private double emissionsKg;

    public String getId() {
        return id;
    }

    public DirectMeasure setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public DirectMeasure setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getMeterId() {
        return meterId;
    }

    public DirectMeasure setMeterId(String meterId) {
        this.meterId = meterId;
        return this;
    }

    public int getYear() {
        return year;
    }

    public DirectMeasure setYear(int year) {
        this.year = year;
        return this;
    }

    public int getMonth() {
        return month;
    }

    public DirectMeasure setMonth(int month) {
        this.month = month;
        return this;
    }

    public int getDay() {
        return day;
    }

    public DirectMeasure setDay(int day) {
        this.day = day;
        return this;
    }

    public int getHour() {
        return hour;
    }

    public DirectMeasure setHour(int hour) {
        this.hour = hour;
        return this;
    }

    public int getMinute() {
        return minute;
    }

    public DirectMeasure setMinute(int minute) {
        this.minute = minute;
        return this;
    }

    public int getDuration() {
        return duration;
    }

    public DirectMeasure setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public DirectMeasure setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public double getUsage() {
        return usage;
    }

    public DirectMeasure setUsage(double usage) {
        this.usage = usage;
        return this;
    }

    public double getCost() {
        return cost;
    }

    public DirectMeasure setCost(double cost) {
        this.cost = cost;
        return this;
    }

    public double getUsageKWH() {
        return usageKWH;
    }

    public DirectMeasure setUsageKWH(double usageKWH) {
        this.usageKWH = usageKWH;
        return this;
    }

    public double getEmissionsKg() {
        return emissionsKg;
    }

    public DirectMeasure setEmissionsKg(double emissionsKg) {
        this.emissionsKg = emissionsKg;
        return this;
    }
}
