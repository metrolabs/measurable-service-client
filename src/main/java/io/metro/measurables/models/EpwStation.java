/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.geo.Distance;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EpwStation {
    private String id;
    private String title;
    private String usaf;
    private String epwUrl;
    private String ddyUrl;
    private GeoJsonPoint location;
    private Distance distance;

    public String getId() {
        return id;
    }

    public EpwStation setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public EpwStation setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getUsaf() {
        return usaf;
    }

    public EpwStation setUsaf(String usaf) {
        this.usaf = usaf;
        return this;
    }

    public String getEpwUrl() {
        return epwUrl;
    }

    public EpwStation setEpwUrl(String epwUrl) {
        this.epwUrl = epwUrl;
        return this;
    }

    public String getDdyUrl() {
        return ddyUrl;
    }

    public EpwStation setDdyUrl(String ddyUrl) {
        this.ddyUrl = ddyUrl;
        return this;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public EpwStation setLocation(GeoJsonPoint coordinates) {
        this.location = coordinates;
        return this;
    }

    public Distance getDistance() {
        return distance;
    }

    public EpwStation setDistance(Distance distance) {
        this.distance = distance;
        return this;
    }
}
