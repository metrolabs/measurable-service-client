/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.measurables.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.jscience.economics.money.Currency;

import javax.measure.converter.UnitConverter;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;
import java.util.*;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Measurable {
    private String id;
    private String accountId;
    private String buildingId;
    private TYPES meterType;
    private UNITS unitType;
    private CURRENCY currencyType;
    private String meterName;
    private INCREMENTS incrementType;
    private Boolean metered;
    private Boolean inUse;
    private Double averagePrice;
    private Double annualEmissions;
    private Double emissionsRatio;
    private Double annualConsumption;
    private List<HourlyMeasure> hourly;
    private List<DailyMeasure> daily;
    private List<MonthlyMeasure> monthly;
    private List<YearlyMeasure> yearly;

    public String getId() {
        return id;
    }

    public Measurable setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Measurable setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public Measurable setBuildingId(String buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public TYPES getMeterType() {
        return meterType;
    }

    public Measurable setMeterType(TYPES meterType) {
        this.meterType = meterType;
        return this;
    }

    public UNITS getUnitType() {
        return unitType;
    }

    public Measurable setUnitType(UNITS unitType) {
        this.unitType = unitType;
        return this;
    }

    public CURRENCY getCurrencyType() {
        return currencyType;
    }

    public Measurable setCurrencyType(CURRENCY currencyType) {
        this.currencyType = currencyType;
        return this;
    }

    public String getMeterName() {
        return meterName;
    }

    public Measurable setMeterName(String meterName) {
        this.meterName = meterName;
        return this;
    }

    public INCREMENTS getIncrementType() {
        return incrementType;
    }

    public Measurable setIncrementType(INCREMENTS incrementType) {
        this.incrementType = incrementType;
        return this;
    }

    public Boolean getMetered() {
        return metered;
    }

    public Measurable setMetered(Boolean metered) {
        this.metered = metered;
        return this;
    }

    public Boolean getInUse() {
        return inUse;
    }

    public Measurable setInUse(Boolean inUse) {
        this.inUse = inUse;
        return this;
    }

    public Double getAveragePrice() {
        return averagePrice;
    }

    public Measurable setAveragePrice(Double averagePrice) {
        this.averagePrice = averagePrice;
        return this;
    }

    public Double getAnnualEmissions() {
        return annualEmissions;
    }

    public Measurable setAnnualEmissions(Double annualEmissions) {
        this.annualEmissions = annualEmissions;
        return this;
    }

    public Double getEmissionsRatio() {
        return emissionsRatio;
    }

    public Measurable setEmissionsRatio(Double emissionsRatio) {
        this.emissionsRatio = emissionsRatio;
        return this;
    }

    public Double getAnnualConsumption() {
        return annualConsumption;
    }

    public Measurable setAnnualConsumption(Double annualConsumption) {
        this.annualConsumption = annualConsumption;
        return this;
    }

    public List<HourlyMeasure> getHourly() {
        return hourly;
    }

    public Measurable setHourly(List<HourlyMeasure> hourly) {
        this.hourly = hourly;
        return this;
    }

    public List<DailyMeasure> getDaily() {
        return daily;
    }

    public Measurable setDaily(List<DailyMeasure> daily) {
        this.daily = daily;
        return this;
    }

    public List<MonthlyMeasure> getMonthly() {
        return monthly;
    }

    public Measurable setMonthly(List<MonthlyMeasure> monthly) {
        this.monthly = monthly;
        return this;
    }

    public List<YearlyMeasure> getYearly() {
        return yearly;
    }

    public Measurable setYearly(List<YearlyMeasure> yearly) {
        this.yearly = yearly;
        return this;
    }

    public enum TYPES {
        COAL_ANTHRACITE ("Coal Anthracite", Constants.getCarbonaceous(), 0.030388539269038),
        COAL_BITUMINOUS ("Coal Bituminous", Constants.getCarbonaceous(), 0.027337669428256),
        COKE ("Coke", Constants.getCarbonaceous(), 0.033313388549634),
        DISTRICT_CHILLED_WATER_ABSORPTION_NG ("District Chilled Water - Absorption Chiller using Natural Gas", Constants.getDistrictChilledWater(), 0.015550350984812),
        DISTRICT_CHILLED_WATER_ELECTRIC ("District Chilled Water - Electric-Driven Chiller", Constants.getDistrictChilledWater(), 0d),
        DISTRICT_CHILLED_WATER_ENGINE_NG ("District Chilled Water - Engine-Driven Chiller using Natural Gas", Constants.getDistrictChilledWater(), 0.015550350984812),
        DISTRICT_CHILLED_WATER_OTHER ("District Chilled Water - Other", Constants.getDistrictChilledWater(), 0d),
        DISTRICT_HOT_WATER ("District Hot Water", Constants.getDistrictHotWater(), 0d),
        DISTRICT_STEAM ("District Steam", Constants.getDistrictSteam(), 0d),
        ELECTRIC ("Grid Electric", Constants.getElectricity(), 0d),
        ELECTRIC_SOLAR ("Electric on Site Solar", Constants.getElectricity(), 0d),
        ELECTRIC_WIND ("Electric on Site Wind", Constants.getElectricity(), 0d),
        IT_EQUIPMENT_ENERGY ("IT Equipment Input Energy (meters on each piece of equipment)", Constants.getElectricity(), 0d),
        PDU_INPUT ("Power Distribution Unit (PDU) Input Energy", Constants.getElectricity(), 0d),
        PDU_OUTPUT ("Power Distribution Unit (PDU) Output Energy", Constants.getElectricity(), 0d),
        UPS_OUTPUT ("Uninterruptible Power Supply (UPS) Output Energy", Constants.getElectricity(), 0d),
        FUEL_OIL_1 ("Fuel Oil No 1", Constants.getFuel(), 0.02146745589215),
        FUEL_OIL_2 ("Fuel Oil No 2", Constants.getFuel(), 0.021675536351992),
        FUEL_OIL_4 ("Fuel Oil No 4", Constants.getFuel(), 0.021992053107808),
        FUEL_OIL_5_6 ("Fuel Oil No 5 or 6", Constants.getFuel(), 0.02200963737202),
        KEROSENE ("Kerosene", Constants.getFuel(), 0.02203894447904),
        DIESEL ("Diesel", Constants.getFuel(), 0.020579450549444),
        NATURAL_GAS ("Natural Gas", Constants.getNaturalGas(), 0.015550350984812),
        OTHER_ENERGY ("Other (Energy)", Constants.getOther(), 0d),
        PROPANE ("Propane", Constants.getPropane(), 0.018012147974492),
        WOOD ("Wood", Constants.getWood(), 0.02749006638476),
        ALT_WATER_ONSITE_MIXED ("Alternative Water Generated On-Site - Mixed Indoor/Outdoor", Constants.getWater(), 0d),
        ALT_WATER_ONSITE_INDOOR ("Alternative Water Generated On-Site - Indoor", Constants.getWater(), 0d),
        ALT_WATER_ONSITE_OUTDOOR ("Alternative Water Generated On-Site - Outdoor", Constants.getWater(), 0d),
        MUNICIPAL_POTABLE_WATER_MIXED ("Municipally Supplied Potable Water - Mixed Indoor/Outdoor", Constants.getWater(), 0d),
        MUNICIPAL_POTABLE_WATER_INDOOR ("Municipally Supplied Potable Water - Indoor", Constants.getWater(), 0d),
        MUNICIPAL_POTABLE_WATER_OUTDOOR ("Municipally Supplied Potable Water - Outdoor", Constants.getWater(), 0d),
        MUNICIPAL_RECLAIMED_WATER_MIXED ("Municipally Supplied Reclaimed Water - Mixed Indoor/Outdoor", Constants.getWater(), 0d),
        MUNICIPAL_RECLAIMED_WATER_INDOOR ("Municipally Supplied Reclaimed Water - Indoor", Constants.getWater(), 0d),
        MUNICIPAL_RECLAIMED_WATER_OUTDOOR ("Municipally Supplied Reclaimed Water - Outdoor", Constants.getWater(), 0d),
        AVERAGE_INFLUENT_FLOW ("Average Influent Flow", Constants.getWater(), 0d),
        OTHER_WATER_MIXED ("Other - Mixed Indoor/Outdoor (Water)", Constants.getWater(), 0d),
        OTHER_OUTDOOR ("Other - Outdoor", Constants.getOther(), 0d),
        OTHER_INDOOR ("Other - Indoor", Constants.getOther(), 0d);

        private String description;
        private Map<String, Constants.Meta> baseUnit;
        private double ghgFactor;

        TYPES(String description, Map<String, Constants.Meta> unit, double ghgFactor) {
            this.description = description;
            this.baseUnit = unit;
            this.ghgFactor = ghgFactor;
        }

        public String getDescription() {
            return description;
        }

        public Map<String, Constants.Meta> getTypeUnits() {
            return baseUnit;
        }

        public double getGhgFactor() {
            return ghgFactor;
        }
    }

    public enum UNITS {
        J       (Constants.J),
        GJ      (Constants.GJ),
        MJ      (Constants.MJ),
        KJ      (Constants.KJ),
        CM      (Constants.CM),
        CF      (Constants.CF),
        CCF     (Constants.CCF),
        KCF     (Constants.KCF),
        MCF     (Constants.MCF),
        LBS     (Constants.LBS),
        KWH     (Constants.KWH),
        MWH     (Constants.MWH),
        GWH     (Constants.GWH),
        KBTU    (Constants.KBTU),
        MBTU    (Constants.MBTU),
        KLBS    (Constants.KLBS),
        MLBS    (Constants.MLBS),
        TONS    (Constants.TONS),
        GAL_US  (Constants.GAL_US),
        LITERS  (Constants.LITERS),
        THERMS  (Constants.THERMS),
        TON_HRS (Constants.TON_HRS);

        private Constants.Meta unit;

        UNITS(Constants.Meta unit) {
            this.unit = unit;
        }

        public Constants.Meta getUnitMeta() {
            return unit;
        }
    }

    public enum CURRENCY {
        AUD("AUD", Currency.AUD),
        CAD("CAD", Currency.CAD),
        CNY("CNY", Currency.CNY),
        EUR("EUR", Currency.EUR),
        GBP("GBP", Currency.GBP),
        JPY("JPY", Currency.JPY),
        KRW("KRW", Currency.KRW),
        TWD("TWD", Currency.TWD),
        USD("USD", Currency.USD);

        private String code = "";
        private Currency currency;

        CURRENCY(String code, Currency currency) {
            this.code = code;
            this.currency = currency;
        }

        public Currency getCurrency() {
            return currency;
        }

        @Override
        public String toString() {
            return code;
        }
    }

    public enum INCREMENTS {
        MINUTE,
        HOUR,
        DAY,
        MONTH
    }

    public static class Constants {
        public static class Meta {
            private String name;
            private Unit<?> baseUnit;
            private Unit<?> fromUnit;

            public Meta(String name, Unit<?> baseUnit, Unit<?> fromUnit) {
                this.name = name;
                this.baseUnit = baseUnit;
                this.fromUnit = fromUnit;
            }

            public String getName() {
                return name;
            }

            public Unit<?> getBaseUnit() {
                return baseUnit;
            }

            public Unit<?> getFromUnit() {
                return fromUnit;
            }

            public double toBase(Number val) {
                UnitConverter converter = fromUnit.getConverterTo(baseUnit);
                return converter.convert(val.doubleValue());
            }

            public double fromBase(Number val) {
                UnitConverter converter = baseUnit.getConverterTo(fromUnit);
                return converter.convert(val.doubleValue());
            }
        }

        private static  Unit<?> _KWH = SI.KILO(SI.WATT).times(NonSI.HOUR);
        private static Map<String, Constants.Meta> UNITS = new HashMap<>();
        static {
            UNITS.put("GJ",         new Constants.Meta("GJ",      _KWH, SI.GIGA(SI.JOULE)));
            UNITS.put("MJ",         new Constants.Meta("MJ",      _KWH, SI.MEGA(SI.JOULE)));
            UNITS.put("KJ",         new Constants.Meta("KJ",      _KWH, SI.KILO(SI.JOULE)));
            UNITS.put("J",          new Constants.Meta("J",       _KWH, SI.JOULE));
            UNITS.put("KBTU",       new Constants.Meta("KBTU",    _KWH, SI.KILO(ImperialUnits.BTU_NG)));
            UNITS.put("MBTU",       new Constants.Meta("MBTU",    _KWH, SI.MEGA(ImperialUnits.BTU_NG)));
            UNITS.put("KWH",        new Constants.Meta("KWH",     _KWH, SI.KILO(SI.WATT).times(NonSI.HOUR)));
            UNITS.put("MWH",        new Constants.Meta("MWH",     _KWH, SI.MEGA(SI.WATT).times(NonSI.HOUR)));
            UNITS.put("GWH",        new Constants.Meta("GWH",     _KWH, SI.GIGA(SI.WATT).times(NonSI.HOUR)));
            UNITS.put("CF",         new Constants.Meta("CF",      _KWH, ImperialUnits.CUBIC_FEET_NG));
            UNITS.put("CCF",        new Constants.Meta("CCF",     _KWH, SI.CENTI(ImperialUnits.CUBIC_FEET_NG)));
            UNITS.put("KCF",        new Constants.Meta("KCF",     _KWH, SI.KILO(ImperialUnits.CUBIC_FEET_NG)));
            UNITS.put("MCF",        new Constants.Meta("MCF",     _KWH, SI.MEGA(ImperialUnits.CUBIC_FEET_NG)));
            UNITS.put("THERMS",     new Constants.Meta("THERMS",  _KWH, ImperialUnits.THERM_NG));
            UNITS.put("CM",         new Constants.Meta("CM",      _KWH, ImperialUnits.CUBIC_METRE_NG));
            UNITS.put("GAL_US",     new Constants.Meta("GAL_US",  _KWH, NonSI.GALLON_LIQUID_US));
            UNITS.put("LITERS",     new Constants.Meta("LITERS",  _KWH, NonSI.LITER));
            UNITS.put("LBS",        new Constants.Meta("LBS",     _KWH, NonSI.POUND));
            UNITS.put("KLBS",       new Constants.Meta("KLBS",    _KWH, SI.KILO(NonSI.POUND)));
            UNITS.put("MLBS",       new Constants.Meta("MLBS",    _KWH, SI.MEGA(NonSI.POUND)));
            UNITS.put("TON_HRS",    new Constants.Meta("TON_HRS", _KWH, NonSI.TON_US.times(NonSI.HOUR)));
            UNITS.put("TONS",       new Constants.Meta("TONS",    _KWH, NonSI.TON_US));
        }

        public static final Constants.Meta GJ      = UNITS.get("GJ");
        public static final Constants.Meta MJ      = UNITS.get("MJ");
        public static final Constants.Meta KJ      = UNITS.get("KJ");
        public static final Constants.Meta J       = UNITS.get("J");
        public static final Constants.Meta KBTU    = UNITS.get("KBTU");
        public static final Constants.Meta MBTU    = UNITS.get("MBTU");
        public static final Constants.Meta KWH     = UNITS.get("KWH");
        public static final Constants.Meta MWH     = UNITS.get("MWH");
        public static final Constants.Meta GWH     = UNITS.get("GWH");
        public static final Constants.Meta CF      = UNITS.get("CF");
        public static final Constants.Meta CCF     = UNITS.get("CCF");
        public static final Constants.Meta KCF     = UNITS.get("KCF");
        public static final Constants.Meta MCF     = UNITS.get("MCF");
        public static final Constants.Meta THERMS  = UNITS.get("THERMS");
        public static final Constants.Meta CM      = UNITS.get("CM");
        public static final Constants.Meta GAL_US  = UNITS.get("GAL_US");
        public static final Constants.Meta LITERS  = UNITS.get("LITERS");
        public static final Constants.Meta LBS     = UNITS.get("LBS");
        public static final Constants.Meta KLBS    = UNITS.get("KLBS");
        public static final Constants.Meta MLBS    = UNITS.get("MLBS");
        public static final Constants.Meta TON_HRS = UNITS.get("TON_HRS");
        public static final Constants.Meta TONS    = UNITS.get("TONS");

        public static Map getUnits() {
            return UNITS;
        }

        public static Constants.Meta getUnit(String unit) {
            return UNITS.get(unit);
        }

        public static Map getElectricity() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(GWH.getName(), GWH);
            UNITS.put(MWH.getName(), MWH);
            UNITS.put(KWH.getName(), KWH);

            return UNITS;
        }

        public static Map getNaturalGas() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(THERMS.getName(), THERMS);
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(MCF.getName(),    MCF);
            UNITS.put(KCF.getName(),    KCF);
            UNITS.put(CCF.getName(),    CCF);
            UNITS.put(CF.getName(),     CF);
            UNITS.put(CM.getName(),     CM);

            return UNITS;
        }

        public static Map getPropane() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(GAL_US.getName(), GAL_US);
            UNITS.put(LITERS.getName(), LITERS);
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(KCF.getName(),    KCF);
            UNITS.put(CF.getName(),     CF);

            return UNITS;
        }

        public static Map getDistrictSteam() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(THERMS.getName(), THERMS);
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(LBS.getName(),    LBS);
            UNITS.put(KLBS.getName(),   KLBS);
            UNITS.put(MLBS.getName(),   MLBS);

            return UNITS;
        }

        public static Map getDistrictHotWater() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(THERMS.getName(), THERMS);
            UNITS.put(TON_HRS.getName(), TON_HRS);

            return UNITS;
        }

        public static Map getDistrictChilledWater() {
            return getDistrictHotWater();
        }

        public static Map getFuel() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(GAL_US.getName(), GAL_US);
            UNITS.put(LITERS.getName(), LITERS);

            return UNITS;
        }

        public static Map getCarbonaceous() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(LBS.getName(),    LBS);
            UNITS.put(KLBS.getName(),   KLBS);
            UNITS.put(MLBS.getName(),   MLBS);
            UNITS.put(TONS.getName(),   TONS);

            return UNITS;
        }

        public static Map getWood() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(MBTU.getName(),   MBTU);
            UNITS.put(KBTU.getName(),   KBTU);
            UNITS.put(TONS.getName(),   TONS);

            return UNITS;
        }

        public static Map getWater() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(GAL_US.getName(), GAL_US);
            UNITS.put(LITERS.getName(), LITERS);

            return UNITS;
        }

        public static Map getOther() {
            Map<String, Constants.Meta> UNITS = new HashMap<>();
            UNITS.put(KBTU.getName(),   KBTU);

            return UNITS;
        }
    }

    public static class META {
        private String key;
        private String description;
        private Collection<String> unitsOfMeasure;

        public META() {}

        public META(String key) {
            TYPES type = TYPES.valueOf(key);

            this.key            = type.name();
            this.description    = type.getDescription();
            this.unitsOfMeasure = type.getTypeUnits().keySet();
        }

        public META(TYPES type) {
            this.key            = type.name();
            this.description    = type.getDescription();
            this.unitsOfMeasure = type.getTypeUnits().keySet();
        }

        public META(String key, String description, Collection<String> unitsOfMeasure) {
            this.key            = key;
            this.description    = description;
            this.unitsOfMeasure = unitsOfMeasure;
        }

        public String getKey() {
            return key;
        }

        public String getDescription() {
            return description;
        }

        public Collection<String> getUnitsOfMeasure() {
            return unitsOfMeasure;
        }

        public static Map<String, META> getMeterTypes() {
            Map<String, META> collection = new TreeMap<>();
            for(TYPES type : TYPES.values()) {
                collection.put(type.name(), new META(type));
            }

            return collection;
        }
    }
}
