/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

import io.metro.measurables.MeasurableClient;
import io.metro.measurables.MetroPagedResources;
import io.metro.measurables.models.ClimateZone;
import io.metro.measurables.models.EpwStation;
import io.metro.measurables.models.Measurable;
import io.metro.measurables.models.NoaaStation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@SpringBootTest(classes={MeasurableClient.class, RestTemplateFilter.class})
public class MeasurableClientTest {
    private static final Logger logger = LoggerFactory.getLogger(MeasurableClientTest.class);

    @Autowired
    private MeasurableClient client;

    private String meterId = "METER1475024473084dHA7oC";
    private String buildingId = "BUILDING1471994530429QNEjxZ";

    @Value("${spring.api.username:xxx}")
    private String username;
    @Value("${spring.api.password:xxx}")
    private String password;

    @Before
    public void before() {
        client.init();
    }

    @Test
    public void getMeter() {
        ResponseEntity<Resource<Measurable>> response = client.getMeasurable(meterId);
        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody().getId().getHref().endsWith(meterId));
    }

    @Test
    public void getMeterForBuildingId() {
        ResponseEntity<MetroPagedResources<Resource<Measurable>>> response = client.getBuildingMeasurables(buildingId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalPages");
        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalElements");
        assertThat(response.getBody()).hasAtLeastOneElementOfType(Resource.class);

    }

    @Test
    public void getBuildingMeterCollection() {
        ResponseEntity<MetroPagedResources<Resource<Measurable>>> response =
                client.getBuildingMeasurables(buildingId, 0, 25);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalPages");
        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalElements");
        assertThat(response.getBody()).hasAtLeastOneElementOfType(Resource.class);
    }



    @Test
    public void getNearestNoaaStation() {
        ResponseEntity<PagedResources<Resource<NoaaStation>>> response =
                client.getNearestNoaaStation(35.78, -78.64);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        NoaaStation station = response.getBody().getContent().iterator().next().getContent();
        assertThat(station.getDistance().getValue()).isLessThan(100);
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getNearestEnergyPlusStation() {
        ResponseEntity<PagedResources<Resource<EpwStation>>> response =
                client.getNearestEnergyPlusStation(35.78, -78.64);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        EpwStation station = response.getBody().getContent().iterator().next().getContent();
        assertThat(station.getDistance().getValue()).isLessThan(100);
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }



    @Test
    public void getNoaaWeatherStation() {
        ResponseEntity<Resource<NoaaStation>> response = client.getNoaaWeatherStation("723060-13722");

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody().getId().getHref().endsWith("723060-13722"));
    }

    @Test
    public void getEnergyPlusWeatherStation() {
        ResponseEntity<Resource<EpwStation>> response =
                client.getEnergyPlusWeatherStation("JPN_Tokyo.Hyakuri.477150_IWEC");

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody().getId().getHref().endsWith("JPN_Tokyo.Hyakuri.477150_IWEC"));
    }



    @Test
    public void findNearestClimateZone() {
        ResponseEntity<PagedResources<Resource<ClimateZone>>> response =
                client.findNearestClimateZone(35.78, -78.64);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        ClimateZone station = response.getBody().getContent().iterator().next().getContent();
        assertThat(station.getDistance().getValue()).isLessThan(100);
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);

    }

    @Test
    public void getClimateZone() {
        ResponseEntity<Resource<ClimateZone>> response =
                client.getClimateZone("8e512ff8-7f6c-4127-beee-805be8ed2c4f");

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        ClimateZone station = response.getBody().getContent();
        assertThat(station.getAshrae()).isEqualToIgnoringCase("2A");
        assertTrue(response.getBody().getId().getHref().endsWith("8e512ff8-7f6c-4127-beee-805be8ed2c4f"));
    }
}
